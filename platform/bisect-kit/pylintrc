# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

[MASTER]
load-plugins=chromite.cli.cros.lint

# Configure quote preferences.
string-quote = single-avoid-escape
triple-quote = double
docstring-quote = double

[MESSAGES CONTROL]
enable=
    apply-builtin,
    backtick,
    bad-python3-import,
    basestring-builtin,
    buffer-builtin,
    #cmp-builtin,
    #cmp-method,
    coerce-builtin,
    coerce-method,
    delslice-method,
    deprecated-itertools-function,
    deprecated-str-translate-call,
    deprecated-string-function,
    deprecated-types-field,
    dict-items-not-iterating,
    dict-iter-method,
    dict-keys-not-iterating,
    dict-values-not-iterating,
    dict-view-method,
    div-method,
    exception-message-attribute,
    execfile-builtin,
    file-builtin,
    filter-builtin-not-iterating,
    getslice-method,
    hex-method,
    idiv-method,
    import-star-module-level,
    indexing-exception,
    input-builtin,
    intern-builtin,
    invalid-str-codec,
    long-builtin,
    #map-builtin-not-iterating,
    metaclass-assignment,
    next-method-called,
    next-method-defined,
    nonzero-method,
    oct-method,
    #old-division,
    old-raise-syntax,
    #parameter-unpacking,
    print-statement,
    raising-string,
    range-builtin-not-iterating,
    raw_input-builtin,
    rdiv-method,
    reduce-builtin,
    reload-builtin,
    setslice-method,
    standarderror-builtin,
    sys-max-int,
    unichr-builtin,
    unicode-builtin,
    unpacking-in-except,
    using-cmp-argument,
    xrange-builtin,
    zip-builtin-not-iterating,

disable=
    broad-except,
    duplicate-code,
    fixme,
    file-ignored,
    invalid-name,
    locally-disabled,
    locally-enabled,
    missing-docstring,
    no-member,
    no-self-use,
    redefined-variable-type,
    relative-import,
    too-few-public-methods,
    too-many-arguments,
    too-many-boolean-expressions,
    too-many-branches,
    too-many-instance-attributes,
    too-many-lines,
    too-many-locals,
    too-many-nested-blocks,
    too-many-public-methods,
    too-many-return-statements,
    too-many-statements,

[REPORTS]
output-format=parseable
reports=no

[FORMAT]
max-line-length=80
indent-string='  '

[BASIC]
bad-functions=
    apply,
    exit,
    filter,
    input,
    #map,
    quit,
    raw_input,
    reduce,


[SIMILARITIES]
min-similarity-lines=20

[VARIABLES]
dummy-variables-rgx=_|unused_

[IMPORTS]
deprecated-modules=
    Bastion,
    cPickle,
    exceptions,
    mox,
    optparse,
    Queue,
    regsub,
    rexec,
    TERMIOS,
