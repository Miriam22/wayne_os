Utvikleralternativer
Vis feilsøkingsinformasjon
Slå på OS-bekreftelse
Slå av
Språk
Start opp fra nettverket
Start opp med Legacy BIOS
Start opp fra USB
Start opp fra USB eller SD-kort
Start opp fra intern disk
Avbryt
Bekreft at du vil slå på OS-bekreftelse
Slå av OS-bekreftelse
Bekreft at du vil slå av OS-bekreftelse
Bruk volumknappene for å navigere opp eller ned
og av/på-knappen for å velge et alternativ.
Hvis du deaktiverer OS-sjekk, blir systemet ditt USIKKERT.
Velg «Avbryt» for å fortsette å være beskyttet.
OS-sjekk er AV. Systemet ditt er USIKKERT.
Velg «Slå på OS-bekreftelse» for å få sikkerheten tilbake.
Velg «Bekreft at du vil slå på OS-bekreftelse» for å beskytte systemet ditt.
