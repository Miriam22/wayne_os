image:
  distribution: "ubuntu"
  release: bionic 
  variant: default
  description: Ubuntu {{ image.release }}
  architecture: amd64

source:
  downloader: ubuntu-http
  url: http://cdimage.ubuntu.com/ubuntu-base
  keys:
    - 0x46181433FBB75451
    - 0xD94AA3F0EFE21092

  apt_sources: |-
    {% if image.architecture_mapped == "amd64" or image.architecture_mapped == "i386" %}deb http://archive.ubuntu.com/ubuntu {{ image.release }} main restricted universe multiverse
    deb http://archive.ubuntu.com/ubuntu {{ image.release }}-updates main restricted universe multiverse
    deb http://security.ubuntu.com/ubuntu {{ image.release }}-security main restricted universe multiverse
    {% else %}deb http://ports.ubuntu.com/ubuntu-ports {{ image.release }} main restricted universe multiverse
    deb http://ports.ubuntu.com/ubuntu-ports {{ image.release }}-updates main restricted universe multiverse
    deb http://ports.ubuntu.com/ubuntu-ports {{ image.release }}-security main restricted universe multiverse
    {% endif %}

targets:
  lxc:
    create-message: |-
      You just created an {{ image.description }} container.
    config:
      - type: all
        before: 5
        content: |-
          lxc.include = LXC_TEMPLATE_CONFIG/ubuntu.common.conf

      - type: all
        after: 4
        content: |-
          lxc.include = LXC_TEMPLATE_CONFIG/common.conf

          lxc.mount.entry = /sys/kernel/debug sys/kernel/debug none bind,optional 0 0
          lxc.mount.entry = /sys/kernel/security sys/kernel/security none bind,optional 0 0
          lxc.mount.entry = /sys/fs/pstore sys/fs/pstore none bind,optional 0 0
          lxc.mount.entry = mqueue dev/mqueue mqueue rw,relatime,create=dir,optional 0 0

      - type: all
        content: |-
          lxc.arch = {{ image.architecture_personality }}

      - type: all
        content: |-
          # Network configuration
          lxc.net.0.type = veth
          lxc.net.0.flags = up
          lxc.net.0.link = lxcbr0
          lxc.net.0.hwaddr = 00:16:3e:xx:xx:xx

files:
 - path: /etc/hostname
   generator: hostname

 - path: /etc/hosts
   generator: hosts

 - path: /etc/resolvconf/resolv.conf.d/original
   generator: remove

 - path: /etc/resolvconf/resolv.conf.d/tail
   generator: remove

 - path: /etc/machine-id
   generator: remove

 - path: /etc/netplan/10-lxc.yaml
   generator: dump
   content: |-
     network:
       version: 2
       renderer: NetworkManager
   releases:
     - artful
     - bionic
     - cosmic
     - disco


 - path: /etc/systemd/system/dhclient.service
   generator: dump
   content: |-
     [Unit]
     Description=DHCP Client
     Documentation=man:dhclient(8)
     Wants=network.target
     Before=network.target

     [Service]
     Type=forking
     PIDFile=/var/run/dhclient.pid
     ExecStart=/sbin/dhclient -4 -q

     [Install]
     WantedBy=multi-user.target

 - path: /usr/local/bin/check_network
   generator: dump
   content: |-
     #!/bin/bash

     set -x

     systemctl is-system-running --wait
     touch /var/log/networkcheck
     echo "Starting network check" >> /var/log/networkcheck
     counter=1
     fail=1
     while true; do
       echo "fail = $fail, count = $counter" >> /var/log/networkcheck
       sleep 10
       curl --head -s https://storage.googleapis.com/abci-ssp/autotest-containers/moblab_base_09.tar.xz >/dev/null 2>&1
       status=$?
       if [ $status -ne 0 ]; then
         fail=$((fail+1))
         if [ $fail -gt 2 ] && [ -f /usr/local/reboot_enabled ]; then
           echo "Restarting network manager" >> /var/log/networkcheck
           reboot
         fi
       else
         fail=1
         counter=$((counter+1))
       fi
       sleep $(( ( RANDOM % 10 )  + 1 ))
     done
     echo "Network is up exiting" >> /var/log/networkcheck


 - path: /etc/systemd/system/networkup.service
   generator: dump
   content: |-
     [Unit]
     Description=NM restart if networing not up
     After=network-manager.target

     [Service]
     Type=simple
     PIDFile=/var/run/nmup.pid
     ExecStart=/usr/local/bin/check_network

     [Install]
     WantedBy=multi-user.target


packages:
  manager: apt
  update: true
  cleanup: true

  sets:
    - packages:
      - apt-transport-https
      - language-pack-en
      - openssh-client
      - sudo
      - network-manager
      - netplan.io
      - systemd
      - iputils-ping
      - dbus
      action: install

actions:
  - trigger: post-packages
    action: |-
      #!/bin/sh
      set -eux


      # Make sure the locale is built and functional
      locale-gen en_US.UTF-8
      update-locale LANG=en_US.UTF-8

      # Cleanup underlying /run
      mount -o bind / /mnt
      rm -rf /mnt/run/*
      umount /mnt

      # Cleanup temporary shadow paths
      rm /etc/*-

actions:
  - trigger: post-files
    action: |-
      #!/bin/sh
      set -eux
      chmod uog+x /usr/local/bin/check_network

mappings:
  architecture_map: debian
