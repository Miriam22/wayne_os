This is the Google touchbot controller module in python.

The name was chosen by Toshiba Machine Co., LTD. and is based on the
controller name.  The full official designation of robots with this
controller is:

	ROIBOT
	BA series
	Controller
	MODEL: CA10-M00
	Toshiba Machine Co, Ltd.

Similar robot controllers:

	BA Series:	CA10-M00
			CA10-M01
			CA10-M10
			CA10-M40
	BA II Series:	CA10-M00B
			CA10-M01B
			CA20-M10
			CA20-M40

This module should have utility for all users of robots with these
controller type, although the specific submodules, other than 'error',
will likely not be applicable to robots other than the Google touchbot.

The directory structure is laid out as follows:

	README		This file

	documentation/	More detailed documentation not covered by pydoc.

	gestures/	A set of gesture programs utilizing the roibot/
			library to implement program commands and allow
			interactive control of the robot finger positions,
			return to origin, and so on.

	roibot/		The control module itself.

	roibot/devices/	A set of textual descriptions and coordinate
			bounding box regions for devices the robot is
			prepared to operate on.

	test/		Test support for calidation of the control
			module itself.  This does not contain the
			actual touchpad tests.

Use this module by putting the "roibot" directory into your PYTHONPATH
to make it available.

	import sys
	import roibot

	robot = roibot.ROIbot(sys.argv[1])
	print roibot.error.statusReport(robot)
	...

See examples, pydoc on the module, and documentation subdirectory for
more information.

End Of Document.
