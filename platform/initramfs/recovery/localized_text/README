This directory contains translations for the message strings shown in the
recovery installer UI.

The message translation process works as follows:

1. If you make a code change that requires message changes, adjust the English
strings in the cros_recovery.grd file.

2. To trigger translation of updated messages, take the the current version of
cros_recovery.grd and update the copy in Google's internal code repository.

3. Wait until the actual translation process finishes.

4. Grab the resulting cros_recovery_LANG.xtb files which contain the translated
strings.  Make sure you get the versions that use the correct fingerprinting
algorithm to generate message IDs.  Create a code review that updates the
corresponding files in this directory.  Do a sanity check to verify that your
review only contains string changes (i.e. no unexpected message ID changes).

5. (Optional, but recommended)  Build a recovery image with your translation
update change present and install the recovery image on a device to verify the
translated strings show up as intended.

6. Have your code change reviewed and submit it as usual.
