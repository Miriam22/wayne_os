# License: Apache License 2.0
dulwich==0.19.11

# License: Apache License 2.0
google-endpoints==4.8.0

# License: Apache License 2.0
google-endpoints-api-management==1.11.0

# License: Apache License 2.0
GoogleAppEngineCloudStorageClient==1.9.22.1

# License: MIT
urllib3==1.24.2

# 3-Clause BSD License
protobuf==3.7.0
