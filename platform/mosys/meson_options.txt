option(
  'arch',
  type : 'combo',
  value: 'x86',
  choices: ['x86', 'x86_64', 'amd64', 'arm', 'arm64'],
  description: 'The target architecture.',
)
option(
  'use_cros_config',
  type: 'boolean',
  value: 'false',
  description: 'If set to true, cros_config will be used',
)

option(
  'cros_config_data_src',
  type: 'string',
  description: 'cros_config_data source file',
)
