From a4b5732e23ec9cd4e9624e3c31266bdd39440a40 Mon Sep 17 00:00:00 2001
From: Kan Yan <kyan@google.com>
Date: Thu, 28 Dec 2017 14:19:17 -0800
Subject: [PATCH] tc: add support for new qdisc "arl"

BUG=b:72745632
TEST="tc qdisc add dev wan0 root handle 1: arl minrate 4800kbit "
	"buffer 5000 limit 500 latency 100ms latency_hysteresis 30ms"

Signed-off-by: Kan Yan <kyan@google.com>
---
 include/linux/pkt_sched.h |  23 ++++-
 tc/Makefile               |   1 +
 tc/q_arl.c                | 188 ++++++++++++++++++++++++++++++++++++++
 3 files changed, 211 insertions(+), 1 deletion(-)
 create mode 100644 tc/q_arl.c

diff --git a/include/linux/pkt_sched.h b/include/linux/pkt_sched.h
index 2cf434a..cd7db97 100644
--- a/include/linux/pkt_sched.h
+++ b/include/linux/pkt_sched.h
@@ -710,9 +710,30 @@ enum {
 	TCA_CBQ_POLICE,
 	__TCA_CBQ_MAX,
 };
-
 #define TCA_CBQ_MAX	(__TCA_CBQ_MAX - 1)
 
+/* ARL section */
+
+struct tc_arl_xstats {
+	__u32	max_bw;		/* The maximum bw measured */
+	__u32	min_rate;	/* The lowest base rate */
+	__u32	current_rate;	/* The current rate */
+	__u32	latency;	/* The current latency */
+};
+
+enum {
+	TCA_ARL_UNSPEC,
+	TCA_ARL_BUFFER,
+	TCA_ARL_MIN_RATE,
+	TCA_ARL_MAX_BW,
+	TCA_ARL_LIMIT,
+	TCA_ARL_MAX_LATENCY,
+	TCA_ARL_LATENCY_HYSTERESIS,
+	TCA_ARL_PAD,
+	__TCA_ARL_MAX,
+};
+#define TCA_ARL_MAX (__TCA_ARL_MAX - 1)
+
 /* dsmark section */
 
 enum {
diff --git a/tc/Makefile b/tc/Makefile
index 1a38c02..90f41e4 100644
--- a/tc/Makefile
+++ b/tc/Makefile
@@ -69,6 +69,7 @@ TCMODULES += q_nss.o
 TCMODULES += q_clsact.o
 TCMODULES += e_bpf.o
 TCMODULES += f_matchall.o
+TCMODULES += q_arl.o
 
 TCSO :=
 ifeq ($(TC_CONFIG_ATM),y)
diff --git a/tc/q_arl.c b/tc/q_arl.c
new file mode 100644
index 0000000..ad2bdfb
--- /dev/null
+++ b/tc/q_arl.c
@@ -0,0 +1,188 @@
+/*
+ * This program is free software; you can redistribute it and/or
+ * modify it under the terms of the GNU General Public License
+ * as published by the Free Software Foundation; either version 2
+ * of the License.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * Copyright 2018 Google LLC.
+ * Author:	Kan Yan <kyan@google.com>
+ *
+ */
+
+#include <stdio.h>
+
+#include "utils.h"
+#include "tc_util.h"
+
+static void explain(void)
+{
+	fprintf(stderr, "Usage: ... arl [ limit PACKETS] [ buffer TIME ]");
+	fprintf(stderr, " [ minrate KBPS ] [ maxbw KBPS ]\n");
+	fprintf(stderr, "               [ latency TIME ] ");
+	fprintf(stderr, "[ latency_hysteresis TIME ]\n");
+}
+
+static int arl_parse_opt(struct qdisc_util *qu, int argc, char **argv,
+			 struct nlmsghdr *n)
+{
+	unsigned int buffer = 0, limit = 0, latency = 0, latency_hysteresis = 0;
+	__u64 min_rate = 0, max_bw = 0;
+	struct rtattr *tail;
+
+	while (argc > 0) {
+		if (strcmp(*argv, "buffer") == 0) {
+			NEXT_ARG();
+			if (get_time(&buffer, *argv)) {
+				fprintf(stderr, "Illegal \"buffer\"\n");
+				return -1;
+			}
+		} else if (strcmp(*argv, "minrate") == 0) {
+			NEXT_ARG();
+			if (get_rate64(&min_rate, *argv)) {
+				fprintf(stderr, "Illegal \"minrate\"\n");
+				return -1;
+			}
+		} else if (strcmp(*argv, "maxbw") == 0) {
+			NEXT_ARG();
+			if (get_rate64(&max_bw, *argv)) {
+				fprintf(stderr, "Illegal \"max_bw\"\n");
+				return -1;
+			}
+		} else if (strcmp(*argv, "limit") == 0) {
+			NEXT_ARG();
+			if (get_unsigned(&limit, *argv, 0)) {
+				fprintf(stderr, "Illegal \"limit\"\n");
+				return -1;
+			}
+		} else if (strcmp(*argv, "latency") == 0) {
+			NEXT_ARG();
+			if (get_time(&latency, *argv)) {
+				fprintf(stderr, "Illegal \"latency\"\n");
+				return -1;
+			}
+		} else if (strcmp(*argv, "latency_hysteresis") == 0) {
+			NEXT_ARG();
+			if (get_time(&latency_hysteresis, *argv)) {
+				fprintf(stderr,
+					"Illegal \"latency hysteresis\"\n");
+				return -1;
+			}
+		} else if (strcmp(*argv, "help") == 0) {
+			explain();
+			return -1;
+		} else {
+			fprintf(stderr, "arl: unknown parameter \"%s\"\n",
+				*argv);
+			explain();
+			return -1;
+		}
+		argc--; argv++;
+	}
+
+	tail = NLMSG_TAIL(n);
+	addattr_l(n, 1024, TCA_OPTIONS, NULL, 0);
+	if (buffer)
+		addattr_l(n, 1024, TCA_ARL_BUFFER, &buffer, sizeof(__u32));
+	if (min_rate)
+		addattr_l(n, 1024, TCA_ARL_MIN_RATE, &min_rate, sizeof(__u64));
+	if (max_bw)
+		addattr_l(n, 1024, TCA_ARL_MAX_BW, &max_bw, sizeof(__u64));
+	if (limit)
+		addattr_l(n, 1024, TCA_ARL_LIMIT, &limit, sizeof(__u32));
+	if (latency)
+		addattr_l(n, 1024, TCA_ARL_MAX_LATENCY, &latency,
+			  sizeof(__u32));
+	if (latency_hysteresis)
+		addattr_l(n, 1024, TCA_ARL_LATENCY_HYSTERESIS,
+			  &latency_hysteresis, sizeof(__u32));
+
+	tail->rta_len = (void *) NLMSG_TAIL(n) - (void *) tail;
+	return 0;
+}
+
+static int arl_print_opt(struct qdisc_util *qu, FILE *f, struct rtattr *opt)
+{
+	unsigned int buffer, limit, latency, latency_hysteresis;
+	__u64 min_rate = 0, max_bw;
+	struct rtattr *tb[TCA_ARL_MAX + 1];
+
+	SPRINT_BUF(b1);
+
+	if (opt == NULL)
+		return 0;
+
+	parse_rtattr_nested(tb, TCA_ARL_MAX, opt);
+	if (tb[TCA_ARL_MIN_RATE] &&
+	    RTA_PAYLOAD(tb[TCA_ARL_MIN_RATE]) >= sizeof(__u64)) {
+		min_rate = rta_getattr_u64(tb[TCA_ARL_MIN_RATE]);
+		fprintf(f, "minrate %s ", sprint_rate(min_rate, b1));
+	}
+
+	if (tb[TCA_ARL_BUFFER] &&
+	    RTA_PAYLOAD(tb[TCA_ARL_BUFFER]) >= sizeof(__u32)) {
+		buffer = rta_getattr_u32(tb[TCA_ARL_BUFFER]);
+		fprintf(f, "buffer %s ", sprint_time(buffer, b1));
+	}
+
+	if (tb[TCA_ARL_MAX_BW] &&
+	    RTA_PAYLOAD(tb[TCA_ARL_MAX_BW]) >= sizeof(__u64)) {
+		max_bw = rta_getattr_u64(tb[TCA_ARL_MAX_BW]);
+		fprintf(f, "max_bw %s ", sprint_rate(max_bw, b1));
+	}
+
+	if (tb[TCA_ARL_LIMIT] &&
+	    RTA_PAYLOAD(tb[TCA_ARL_LIMIT]) >= sizeof(__u32)) {
+		limit = rta_getattr_u32(tb[TCA_ARL_LIMIT]);
+		fprintf(f, "limit %u ", limit);
+	}
+
+	if (tb[TCA_ARL_MAX_LATENCY] &&
+	    RTA_PAYLOAD(tb[TCA_ARL_MAX_LATENCY]) >= sizeof(__u32)) {
+		latency = rta_getattr_u32(tb[TCA_ARL_MAX_LATENCY]);
+		fprintf(f, "latency %s ", sprint_time(latency, b1));
+	}
+
+	if (tb[TCA_ARL_LATENCY_HYSTERESIS] &&
+	    RTA_PAYLOAD(tb[TCA_ARL_LATENCY_HYSTERESIS]) >= sizeof(__u32)) {
+		latency_hysteresis =
+			rta_getattr_u32(tb[TCA_ARL_LATENCY_HYSTERESIS]);
+		fprintf(f, "latency_hysteresis %s ",
+			sprint_time(latency_hysteresis, b1));
+	}
+	return 0;
+}
+
+static int arl_print_xstats(struct qdisc_util *qu, FILE *f,
+			    struct rtattr *xstats)
+{
+	struct tc_arl_xstats *st;
+
+	if (xstats == NULL)
+		return 0;
+
+	if (RTA_PAYLOAD(xstats) < sizeof(*st))
+		return -1;
+
+	st = RTA_DATA(xstats);
+
+	fprintf(f, " current_rate %uKbit latency %uus", st->current_rate, st->latency);
+	if (st->max_bw)
+		fprintf(f, " max_bw %uKbit", st->max_bw);
+
+	if (st->min_rate)
+		fprintf(f, " min_base_rate %uKbit", st->min_rate);
+
+	return 0;
+}
+
+struct qdisc_util arl_qdisc_util = {
+	.id		= "arl",
+	.parse_qopt	= arl_parse_opt,
+	.print_qopt	= arl_print_opt,
+	.print_xstats	= arl_print_xstats,
+};
-- 
2.23.0.rc1.153.gdeed80330f-goog

