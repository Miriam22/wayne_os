From 20fef903bfbfbf00902ee736545ad8ad2848c005 Mon Sep 17 00:00:00 2001
From: Jacob Lin <jacoblin@google.com>
Date: Thu, 22 Aug 2019 16:32:50 -0700
Subject: libchrome: Don't crash for mismatched histogram parms.

These mismatches shouldn't happen but crashing is a fairly severe
response for the released products. Just report the error.

This CL is for reference only.

List of CLs combined:
- (r552089) Don't crash for mismatched histogram parms.
https://chromium-review.googlesource.com/c/chromium/src/+/1012469/
- (r545333) Do not record expired histograms.
https://chromium-review.googlesource.com/c/chromium/src/+/957437/
- (r503662) Change Pickle::Write* methods to return void.
https://chromium-review.googlesource.com/c/chromium/src/+/677023/

BUG=b:127244647
TEST=emerge-gale libchrome after generating the patch CL

Change-Id: Ibc8c7a23ebb23d5fa1a1946836923ad5aa49d2bf
---
 BUILD.gn                                      |   1 +
 base/metrics/dummy_histogram.cc               | 102 ++++++++++++++++++
 base/metrics/dummy_histogram.h                |  62 +++++++++++
 base/metrics/histogram.cc                     |  55 +++++-----
 base/metrics/histogram.h                      |   4 +-
 base/metrics/histogram_base.cc                |  11 +-
 base/metrics/histogram_base.h                 |   9 +-
 base/metrics/histogram_base_unittest.cc       |  10 +-
 base/metrics/histogram_samples.h              |   2 +
 base/metrics/histogram_unittest.cc            |   9 +-
 .../metrics/persistent_histogram_allocator.cc |   3 +-
 base/metrics/sparse_histogram.cc              |   5 +-
 base/metrics/sparse_histogram.h               |   2 +-
 13 files changed, 227 insertions(+), 48 deletions(-)
 create mode 100644 base/metrics/dummy_histogram.cc
 create mode 100644 base/metrics/dummy_histogram.h

diff --git a/BUILD.gn b/BUILD.gn
index f292ca4..df993ea 100644
--- a/BUILD.gn
+++ b/BUILD.gn
@@ -130,6 +130,7 @@ libbase_sublibs = [
       "base/message_loop/message_pump_glib.cc",
       "base/message_loop/message_pump_libevent.cc",
       "base/metrics/bucket_ranges.cc",
+      "base/metrics/dummy_histogram.cc",
       "base/metrics/field_trial.cc",
       "base/metrics/field_trial_param_associator.cc",
       "base/metrics/metrics_hashes.cc",
diff --git a/base/metrics/dummy_histogram.cc b/base/metrics/dummy_histogram.cc
new file mode 100644
index 0000000..974371d
--- /dev/null
+++ b/base/metrics/dummy_histogram.cc
@@ -0,0 +1,102 @@
+// Copyright 2017 The Chromium Authors. All rights reserved.
+// Use of this source code is governed by a BSD-style license that can be
+// found in the LICENSE file.
+
+#include "base/metrics/dummy_histogram.h"
+
+#include <memory>
+
+#include "base/logging.h"
+#include "base/metrics/histogram_samples.h"
+#include "base/metrics/metrics_hashes.h"
+
+namespace base {
+
+namespace {
+
+// Helper classes for DummyHistogram.
+class DummySampleCountIterator : public SampleCountIterator {
+ public:
+  DummySampleCountIterator() {}
+  ~DummySampleCountIterator() override {}
+
+  // SampleCountIterator:
+  bool Done() const override { return true; }
+  void Next() override { NOTREACHED(); }
+  void Get(HistogramBase::Sample* min,
+           HistogramBase::Sample* max,
+           HistogramBase::Count* count) const override {
+    NOTREACHED();
+  }
+
+ private:
+  DISALLOW_COPY_AND_ASSIGN(DummySampleCountIterator);
+};
+
+class DummyHistogramSamples : public HistogramSamples {
+ public:
+  explicit DummyHistogramSamples() : HistogramSamples(0, new LocalMetadata()) {}
+  ~DummyHistogramSamples() override {
+    delete static_cast<LocalMetadata*>(meta());
+  }
+
+  // HistogramSamples:
+  void Accumulate(HistogramBase::Sample value,
+                  HistogramBase::Count count) override {}
+  HistogramBase::Count GetCount(HistogramBase::Sample value) const override {
+    return HistogramBase::Count();
+  }
+  HistogramBase::Count TotalCount() const override {
+    return HistogramBase::Count();
+  }
+  std::unique_ptr<SampleCountIterator> Iterator() const override {
+    return std::make_unique<DummySampleCountIterator>();
+  }
+  bool AddSubtractImpl(SampleCountIterator* iter, Operator op) override {
+    return true;
+  }
+
+ private:
+  DISALLOW_COPY_AND_ASSIGN(DummyHistogramSamples);
+};
+
+}  // namespace
+
+// static
+DummyHistogram* DummyHistogram::GetInstance() {
+  static base::NoDestructor<DummyHistogram> dummy_histogram;
+  return dummy_histogram.get();
+}
+
+uint64_t DummyHistogram::name_hash() const {
+  return HashMetricName(histogram_name());
+}
+
+HistogramType DummyHistogram::GetHistogramType() const {
+  return DUMMY_HISTOGRAM;
+}
+
+bool DummyHistogram::HasConstructionArguments(
+    Sample expected_minimum,
+    Sample expected_maximum,
+    uint32_t expected_bucket_count) const {
+  return true;
+}
+
+bool DummyHistogram::AddSamplesFromPickle(PickleIterator* iter) {
+  return true;
+}
+
+std::unique_ptr<HistogramSamples> DummyHistogram::SnapshotSamples() const {
+  return std::make_unique<DummyHistogramSamples>();
+}
+
+std::unique_ptr<HistogramSamples> DummyHistogram::SnapshotDelta() {
+  return std::make_unique<DummyHistogramSamples>();
+}
+
+std::unique_ptr<HistogramSamples> DummyHistogram::SnapshotFinalDelta() const {
+  return std::make_unique<DummyHistogramSamples>();
+}
+
+}  // namespace base
diff --git a/base/metrics/dummy_histogram.h b/base/metrics/dummy_histogram.h
new file mode 100644
index 0000000..ebe58fb
--- /dev/null
+++ b/base/metrics/dummy_histogram.h
@@ -0,0 +1,62 @@
+// Copyright 2017 The Chromium Authors. All rights reserved.
+// Use of this source code is governed by a BSD-style license that can be
+// found in the LICENSE file.
+
+#ifndef BASE_METRICS_DUMMY_HISTOGRAM_H_
+#define BASE_METRICS_DUMMY_HISTOGRAM_H_
+
+#include <stdint.h>
+
+#include <memory>
+#include <string>
+
+#include "base/base_export.h"
+#include "base/metrics/histogram_base.h"
+#include "base/no_destructor.h"
+
+namespace base {
+
+// DummyHistogram is used for mocking histogram objects for histograms that
+// shouldn't be recorded. It doesn't do any actual processing.
+
+class BASE_EXPORT DummyHistogram : public HistogramBase {
+ public:
+  static DummyHistogram* GetInstance();
+
+  // HistogramBase:
+  void CheckName(const StringPiece& name) const override {}
+  uint64_t name_hash() const override;
+  HistogramType GetHistogramType() const override;
+  bool HasConstructionArguments(Sample expected_minimum,
+                                Sample expected_maximum,
+                                uint32_t expected_bucket_count) const override;
+  void Add(Sample value) override {}
+  void AddCount(Sample value, int count) override {}
+  void AddSamples(const HistogramSamples& samples) override {}
+  bool AddSamplesFromPickle(PickleIterator* iter) override;
+  std::unique_ptr<HistogramSamples> SnapshotSamples() const override;
+  std::unique_ptr<HistogramSamples> SnapshotDelta() override;
+  std::unique_ptr<HistogramSamples> SnapshotFinalDelta() const override;
+  void WriteHTMLGraph(std::string* output) const override {}
+  void WriteAscii(std::string* output) const override {}
+
+ protected:
+  // HistogramBase:
+  void SerializeInfoImpl(Pickle* pickle) const override {}
+  void GetParameters(DictionaryValue* params) const override {}
+  void GetCountAndBucketData(Count* count,
+                             int64_t* sum,
+                             ListValue* buckets) const override {}
+
+ private:
+  friend class NoDestructor<DummyHistogram>;
+
+  DummyHistogram() : HistogramBase("dummy_histogram") {}
+  ~DummyHistogram() override {}
+
+  DISALLOW_COPY_AND_ASSIGN(DummyHistogram);
+};
+
+}  // namespace base
+
+#endif  // BASE_METRICS_DUMMY_HISTOGRAM_H_
diff --git a/base/metrics/histogram.cc b/base/metrics/histogram.cc
index 16e36ae..f46122e 100644
--- a/base/metrics/histogram.cc
+++ b/base/metrics/histogram.cc
@@ -19,6 +19,7 @@
 #include "base/debug/alias.h"
 #include "base/logging.h"
 #include "base/memory/ptr_util.h"
+#include "base/metrics/dummy_histogram.h"
 #include "base/metrics/histogram_macros.h"
 #include "base/metrics/metrics_hashes.h"
 #include "base/metrics/persistent_histogram_allocator.h"
@@ -72,6 +73,11 @@ bool ReadHistogramArguments(PickleIterator* iter,
 
 bool ValidateRangeChecksum(const HistogramBase& histogram,
                            uint32_t range_checksum) {
+  // Normally, |histogram| should have type HISTOGRAM or be inherited from it.
+  // However, if it's expired, it will actually be a DUMMY_HISTOGRAM.
+  // Skip the checks in that case.
+  if (histogram.GetHistogramType() == DUMMY_HISTOGRAM)
+    return true;
   const Histogram& casted_histogram =
       static_cast<const Histogram&>(histogram);
 
@@ -217,17 +223,17 @@ HistogramBase* Histogram::Factory::Build() {
     ReportHistogramActivity(*histogram, HISTOGRAM_LOOKUP);
   }
 
-  CHECK_EQ(histogram_type_, histogram->GetHistogramType()) << name_;
-  if (bucket_count_ != 0 &&
-      !histogram->HasConstructionArguments(minimum_, maximum_, bucket_count_)) {
+  if (histogram_type_ != histogram->GetHistogramType() ||
+      (bucket_count_ != 0 && !histogram->HasConstructionArguments(
+                                 minimum_, maximum_, bucket_count_))) {
     // The construction arguments do not match the existing histogram.  This can
     // come about if an extension updates in the middle of a chrome run and has
-    // changed one of them, or simply by bad code within Chrome itself.  We
-    // return NULL here with the expectation that bad code in Chrome will crash
-    // on dereference, but extension/Pepper APIs will guard against NULL and not
-    // crash.
-    DLOG(ERROR) << "Histogram " << name_ << " has bad construction arguments";
-    return nullptr;
+    // changed one of them, or simply by bad code within Chrome itself. A NULL
+    // return would cause Chrome to crash; better to just record it for later
+    // analysis.
+    DLOG(ERROR) << "Histogram " << name_
+                << " has mismatched construction arguments";
+    return DummyHistogram::GetInstance();
   }
   return histogram;
 }
@@ -487,14 +493,14 @@ void Histogram::WriteAscii(std::string* output) const {
   WriteAsciiImpl(true, "\n", output);
 }
 
-bool Histogram::SerializeInfoImpl(Pickle* pickle) const {
+void Histogram::SerializeInfoImpl(Pickle* pickle) const {
   DCHECK(bucket_ranges()->HasValidChecksum());
-  return pickle->WriteString(histogram_name()) &&
-      pickle->WriteInt(flags()) &&
-      pickle->WriteInt(declared_min()) &&
-      pickle->WriteInt(declared_max()) &&
-      pickle->WriteUInt32(bucket_count()) &&
-      pickle->WriteUInt32(bucket_ranges()->checksum());
+  pickle->WriteString(histogram_name());
+  pickle->WriteInt(flags());
+  pickle->WriteInt(declared_min());
+  pickle->WriteInt(declared_max());
+  pickle->WriteUInt32(bucket_count());
+  pickle->WriteUInt32(bucket_ranges()->checksum());
 }
 
 Histogram::Histogram(const std::string& name,
@@ -758,6 +764,11 @@ class LinearHistogram::Factory : public Histogram::Factory {
 
   void FillHistogram(HistogramBase* base_histogram) override {
     Histogram::Factory::FillHistogram(base_histogram);
+    // Normally, |base_histogram| should have type LINEAR_HISTOGRAM or be
+    // inherited from it. However, if it's expired, it will actually be a
+    // DUMMY_HISTOGRAM. Skip filling in that case.
+    if (base_histogram->GetHistogramType() == DUMMY_HISTOGRAM)
+      return;
     LinearHistogram* histogram = static_cast<LinearHistogram*>(base_histogram);
     // Set range descriptions.
     if (descriptions_) {
@@ -1124,17 +1135,13 @@ CustomHistogram::CustomHistogram(const std::string& name,
                 meta,
                 logged_meta) {}
 
-bool CustomHistogram::SerializeInfoImpl(Pickle* pickle) const {
-  if (!Histogram::SerializeInfoImpl(pickle))
-    return false;
+void CustomHistogram::SerializeInfoImpl(Pickle* pickle) const {
+  Histogram::SerializeInfoImpl(pickle);
 
   // Serialize ranges. First and last ranges are alwasy 0 and INT_MAX, so don't
   // write them.
-  for (uint32_t i = 1; i < bucket_ranges()->bucket_count(); ++i) {
-    if (!pickle->WriteInt(bucket_ranges()->range(i)))
-      return false;
-  }
-  return true;
+  for (uint32_t i = 1; i < bucket_ranges()->bucket_count(); ++i)
+    pickle->WriteInt(bucket_ranges()->range(i));
 }
 
 double CustomHistogram::GetBucketSize(Count current, uint32_t i) const {
diff --git a/base/metrics/histogram.h b/base/metrics/histogram.h
index a76dd63..e72d420 100644
--- a/base/metrics/histogram.h
+++ b/base/metrics/histogram.h
@@ -237,7 +237,7 @@ class BASE_EXPORT Histogram : public HistogramBase {
             HistogramSamples::Metadata* logged_meta);
 
   // HistogramBase implementation:
-  bool SerializeInfoImpl(base::Pickle* pickle) const override;
+  void SerializeInfoImpl(base::Pickle* pickle) const override;
 
   // Method to override to skip the display of the i'th bucket if it's empty.
   virtual bool PrintEmptyBucket(uint32_t index) const;
@@ -528,7 +528,7 @@ class BASE_EXPORT CustomHistogram : public Histogram {
                   HistogramSamples::Metadata* logged_meta);
 
   // HistogramBase implementation:
-  bool SerializeInfoImpl(base::Pickle* pickle) const override;
+  void SerializeInfoImpl(base::Pickle* pickle) const override;
 
   double GetBucketSize(Count current, uint32_t i) const override;
 
diff --git a/base/metrics/histogram_base.cc b/base/metrics/histogram_base.cc
index 671cad2..91e0b36 100644
--- a/base/metrics/histogram_base.cc
+++ b/base/metrics/histogram_base.cc
@@ -34,6 +34,8 @@ std::string HistogramTypeToString(HistogramType type) {
       return "CUSTOM_HISTOGRAM";
     case SPARSE_HISTOGRAM:
       return "SPARSE_HISTOGRAM";
+    case DUMMY_HISTOGRAM:
+      return "DUMMY_HISTOGRAM";
   }
   NOTREACHED();
   return "UNKNOWN";
@@ -91,10 +93,9 @@ void HistogramBase::AddBoolean(bool value) {
   Add(value ? 1 : 0);
 }
 
-bool HistogramBase::SerializeInfo(Pickle* pickle) const {
-  if (!pickle->WriteInt(GetHistogramType()))
-    return false;
-  return SerializeInfoImpl(pickle);
+void HistogramBase::SerializeInfo(Pickle* pickle) const {
+  pickle->WriteInt(GetHistogramType());
+  SerializeInfoImpl(pickle);
 }
 
 uint32_t HistogramBase::FindCorruption(const HistogramSamples& samples) const {
@@ -211,6 +212,8 @@ void HistogramBase::ReportHistogramActivity(const HistogramBase& histogram,
         case SPARSE_HISTOGRAM:
           report_type = HISTOGRAM_REPORT_TYPE_SPARSE;
           break;
+        case DUMMY_HISTOGRAM:
+          return;
       }
       report_histogram_->Add(report_type);
       if (flags & kIsPersistent)
diff --git a/base/metrics/histogram_base.h b/base/metrics/histogram_base.h
index 4f5ba04..c8d4089 100644
--- a/base/metrics/histogram_base.h
+++ b/base/metrics/histogram_base.h
@@ -39,6 +39,7 @@ enum HistogramType {
   BOOLEAN_HISTOGRAM,
   CUSTOM_HISTOGRAM,
   SPARSE_HISTOGRAM,
+  DUMMY_HISTOGRAM,
 };
 
 std::string HistogramTypeToString(HistogramType type);
@@ -138,10 +139,10 @@ class BASE_EXPORT HistogramBase {
 
   const std::string& histogram_name() const { return histogram_name_; }
 
-  // Comapres |name| to the histogram name and triggers a DCHECK if they do not
+  // Compares |name| to the histogram name and triggers a DCHECK if they do not
   // match. This is a helper function used by histogram macros, which results in
   // in more compact machine code being generated by the macros.
-  void CheckName(const StringPiece& name) const;
+  virtual void CheckName(const StringPiece& name) const;
 
   // Get a unique ID for this histogram's samples.
   virtual uint64_t name_hash() const = 0;
@@ -179,7 +180,7 @@ class BASE_EXPORT HistogramBase {
   // Serialize the histogram info into |pickle|.
   // Note: This only serializes the construction arguments of the histogram, but
   // does not serialize the samples.
-  bool SerializeInfo(base::Pickle* pickle) const;
+  void SerializeInfo(base::Pickle* pickle) const;
 
   // Try to find out data corruption from histogram and the samples.
   // The returned value is a combination of Inconsistency enum.
@@ -224,7 +225,7 @@ class BASE_EXPORT HistogramBase {
   enum ReportActivity { HISTOGRAM_CREATED, HISTOGRAM_LOOKUP };
 
   // Subclasses should implement this function to make SerializeInfo work.
-  virtual bool SerializeInfoImpl(base::Pickle* pickle) const = 0;
+  virtual void SerializeInfoImpl(base::Pickle* pickle) const = 0;
 
   // Writes information about the construction parameters in |params|.
   virtual void GetParameters(DictionaryValue* params) const = 0;
diff --git a/base/metrics/histogram_base_unittest.cc b/base/metrics/histogram_base_unittest.cc
index 1eb8fd4..abf4d2a 100644
--- a/base/metrics/histogram_base_unittest.cc
+++ b/base/metrics/histogram_base_unittest.cc
@@ -50,7 +50,7 @@ TEST_F(HistogramBaseTest, DeserializeHistogram) {
       HistogramBase::kIPCSerializationSourceFlag));
 
   Pickle pickle;
-  ASSERT_TRUE(histogram->SerializeInfo(&pickle));
+  histogram->SerializeInfo(&pickle);
 
   PickleIterator iter(pickle);
   HistogramBase* deserialized = DeserializeHistogramInfo(&iter);
@@ -75,7 +75,7 @@ TEST_F(HistogramBaseTest, DeserializeLinearHistogram) {
       HistogramBase::kIPCSerializationSourceFlag);
 
   Pickle pickle;
-  ASSERT_TRUE(histogram->SerializeInfo(&pickle));
+  histogram->SerializeInfo(&pickle);
 
   PickleIterator iter(pickle);
   HistogramBase* deserialized = DeserializeHistogramInfo(&iter);
@@ -97,7 +97,7 @@ TEST_F(HistogramBaseTest, DeserializeBooleanHistogram) {
       "TestHistogram", HistogramBase::kIPCSerializationSourceFlag);
 
   Pickle pickle;
-  ASSERT_TRUE(histogram->SerializeInfo(&pickle));
+  histogram->SerializeInfo(&pickle);
 
   PickleIterator iter(pickle);
   HistogramBase* deserialized = DeserializeHistogramInfo(&iter);
@@ -124,7 +124,7 @@ TEST_F(HistogramBaseTest, DeserializeCustomHistogram) {
       "TestHistogram", ranges, HistogramBase::kIPCSerializationSourceFlag);
 
   Pickle pickle;
-  ASSERT_TRUE(histogram->SerializeInfo(&pickle));
+  histogram->SerializeInfo(&pickle);
 
   PickleIterator iter(pickle);
   HistogramBase* deserialized = DeserializeHistogramInfo(&iter);
@@ -146,7 +146,7 @@ TEST_F(HistogramBaseTest, DeserializeSparseHistogram) {
       "TestHistogram", HistogramBase::kIPCSerializationSourceFlag);
 
   Pickle pickle;
-  ASSERT_TRUE(histogram->SerializeInfo(&pickle));
+  histogram->SerializeInfo(&pickle);
 
   PickleIterator iter(pickle);
   HistogramBase* deserialized = DeserializeHistogramInfo(&iter);
diff --git a/base/metrics/histogram_samples.h b/base/metrics/histogram_samples.h
index 93f6d21..b4dbbf9 100644
--- a/base/metrics/histogram_samples.h
+++ b/base/metrics/histogram_samples.h
@@ -115,6 +115,8 @@ class BASE_EXPORT HistogramSamples {
   void IncreaseSum(int64_t diff);
   void IncreaseRedundantCount(HistogramBase::Count diff);
 
+  Metadata* meta() { return meta_; }
+
  private:
   // In order to support histograms shared through an external memory segment,
   // meta values may be the local storage or external storage depending on the
diff --git a/base/metrics/histogram_unittest.cc b/base/metrics/histogram_unittest.cc
index 02ed93b..39c0d43 100644
--- a/base/metrics/histogram_unittest.cc
+++ b/base/metrics/histogram_unittest.cc
@@ -15,6 +15,7 @@
 
 #include "base/logging.h"
 #include "base/metrics/bucket_ranges.h"
+#include "base/metrics/dummy_histogram.h"
 #include "base/metrics/histogram_macros.h"
 #include "base/metrics/persistent_histogram_allocator.h"
 #include "base/metrics/persistent_memory_allocator.h"
@@ -622,10 +623,10 @@ TEST_P(HistogramTest, BadConstruction) {
   // Try to get the same histogram name with different arguments.
   HistogramBase* bad_histogram = Histogram::FactoryGet(
       "BadConstruction", 0, 100, 7, HistogramBase::kNoFlags);
-  EXPECT_EQ(NULL, bad_histogram);
+  EXPECT_EQ(DummyHistogram::GetInstance(), bad_histogram);
   bad_histogram = Histogram::FactoryGet(
       "BadConstruction", 0, 99, 8, HistogramBase::kNoFlags);
-  EXPECT_EQ(NULL, bad_histogram);
+  EXPECT_EQ(DummyHistogram::GetInstance(), bad_histogram);
 
   HistogramBase* linear_histogram = LinearHistogram::FactoryGet(
       "BadConstructionLinear", 0, 100, 8, HistogramBase::kNoFlags);
@@ -634,10 +635,10 @@ TEST_P(HistogramTest, BadConstruction) {
   // Try to get the same histogram name with different arguments.
   bad_histogram = LinearHistogram::FactoryGet(
       "BadConstructionLinear", 0, 100, 7, HistogramBase::kNoFlags);
-  EXPECT_EQ(NULL, bad_histogram);
+  EXPECT_EQ(DummyHistogram::GetInstance(), bad_histogram);
   bad_histogram = LinearHistogram::FactoryGet(
       "BadConstructionLinear", 10, 100, 8, HistogramBase::kNoFlags);
-  EXPECT_EQ(NULL, bad_histogram);
+  EXPECT_EQ(DummyHistogram::GetInstance(), bad_histogram);
 }
 
 TEST_P(HistogramTest, FactoryTime) {
diff --git a/base/metrics/persistent_histogram_allocator.cc b/base/metrics/persistent_histogram_allocator.cc
index 5f44b67..d73097b 100644
--- a/base/metrics/persistent_histogram_allocator.cc
+++ b/base/metrics/persistent_histogram_allocator.cc
@@ -668,8 +668,7 @@ PersistentHistogramAllocator::GetOrCreateStatisticsRecorderHistogram(
   // FactoryGet() which will create the histogram in the global persistent-
   // histogram allocator if such is set.
   base::Pickle pickle;
-  if (!histogram->SerializeInfo(&pickle))
-    return nullptr;
+  histogram->SerializeInfo(&pickle);
   PickleIterator iter(pickle);
   existing = DeserializeHistogramInfo(&iter);
   if (!existing)
diff --git a/base/metrics/sparse_histogram.cc b/base/metrics/sparse_histogram.cc
index 415d7f9..738d8e8 100644
--- a/base/metrics/sparse_histogram.cc
+++ b/base/metrics/sparse_histogram.cc
@@ -171,8 +171,9 @@ void SparseHistogram::WriteAscii(std::string* output) const {
   WriteAsciiImpl(true, "\n", output);
 }
 
-bool SparseHistogram::SerializeInfoImpl(Pickle* pickle) const {
-  return pickle->WriteString(histogram_name()) && pickle->WriteInt(flags());
+void SparseHistogram::SerializeInfoImpl(Pickle* pickle) const {
+  pickle->WriteString(histogram_name());
+  pickle->WriteInt(flags());
 }
 
 SparseHistogram::SparseHistogram(const std::string& name)
diff --git a/base/metrics/sparse_histogram.h b/base/metrics/sparse_histogram.h
index 97709ba..d83fe5b 100644
--- a/base/metrics/sparse_histogram.h
+++ b/base/metrics/sparse_histogram.h
@@ -59,7 +59,7 @@ class BASE_EXPORT SparseHistogram : public HistogramBase {
 
  protected:
   // HistogramBase implementation:
-  bool SerializeInfoImpl(base::Pickle* pickle) const override;
+  void SerializeInfoImpl(base::Pickle* pickle) const override;
 
  private:
   // Clients should always use FactoryGet to create SparseHistogram.
-- 
2.23.0.187.g17f5b7556c-goog

