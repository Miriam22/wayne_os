%# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher bar for releases (milestones).
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base=view)

  %#-------------------------------------------------------------------------
  %# The table.
  %#-------------------------------------------------------------------------
  %_tbl = tpl_vars['tbl']
  {{!_tbl.html()}}
%end

%rebase('master.tpl', title=title, query_string=query_string, body_block=body_block)
