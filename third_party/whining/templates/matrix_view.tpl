%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%import datetime

%from src import settings
%_root = settings.settings.relative_root


%# Tooltip used when mouse is over the run numbers.
%def tooltip(fail_set, caption, color, build, platform, crash_count=0, crashes=[]):
  <span style="width:300px">
    %if fail_set:
      <div style="color:{{ color }};">
        <b>{{ caption }}</b><br>
        {{', '.join(list(fail_set)[:30])}}
        %if len(fail_set) > 30:
          <br>
          [truncated]...
        %end
      </div>
      View problem details for {{ build }} on {{ platform }}.
    %else:
      View all test results on {{ platform }}.
    %end
    %if crash_count:
      <div style="color:orange;font-weight:bold;">
        {{ crash_count }} Crashes:</div>
        %for test_name, crash_info in crashes:
          {{ test_name }}:<br>
          %for one_crash in crash_info:
            &nbsp;&nbsp;{{ one_crash }}<br>
          %end
        %end
    %end
  </span>
%end


%def body_block():
  %# --------------------------------------------------------------------------
  %# Releases switcher
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='matrix')

  %# --------------------------------------------------------------------------
  %# Summaries
  <div id="divMatrix" class="centered">
  %_pt = tpl_vars['data']['pt']
  %_builds = tpl_vars['data']['builds']
  %_platforms = tpl_vars['data']['platforms']
  %if not _builds:
    <h3><u>No test results found.</u></h3>
  %else:
    %# -------------------------------------------------------------------------
    %# LEGEND
    %include('matrix_legend.tpl', tpl_vars=tpl_vars, query_string=query_string)

    %# -------------------------------------------------------------------------
    %# Show the main matrix
    <table class="alternate_background" style="width:100%">
      <thead>
        <tr>
          <th class="headeritem centered">Build/Platform</th>
          %if hasattr(_pt.data['build'].values()[0], 'fw_rw_version'):
            <th class="headeritem centered">FW RW Ver</th>
            <th class="headeritem centered">FW RO Ver</th>
            <th class="headeritem centered">Test Ver</th>
          %end
          <th class="headeritem centered">Chrome Ver</th>
          <th class="headeritem centered">Start Time</th>
          %for _platform in _platforms:
            %# Would be cleaner to move tooltip content into celldata class.
            %# Would allow for dynamic table transpose.
            <th class="centered" onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)">
              <a class="tooltip"
                 href="{{ _root }}/platform/{{ tpl_vars['filter_tag'] }}{{! query_string(add={'platforms': _platform}) }}">
                {{ _platform }}
                <span style="width:300px">Show summary for platform={{ _platform }} only.</span>
              </a>
            </th>
          %end
        </tr>
      </thead>
      <tbody>
        %for _build in _builds:
          %_build_cell = _pt.data['build'][_build]
          <tr>
            <th class="headeritem centered">{{ _build }}</td>
            %if hasattr(_pt.data['build'].values()[0], 'fw_rw_version'):
              <th class="centered">{{_build_cell.fw_rw_version}}</th>
              <th class="centered">{{_build_cell.fw_ro_version}}</th>
              <th class="centered">{{_build_cell.test_version}}</th>
            %end
            <th class="centered">{{_build_cell.chromever}}</th>
            <th class="centered">{{_build_cell.strtime}}</th>
            %for _platform in _platforms:
              %_cell = _pt.data['bp'].get( (_build, _platform) )
              %if not _cell:
                <td></td>
                %continue
              %end
              <td>
                <table style="width:100%;">
                  <tbody>
                  <tr>
                  <td class="bordereditem build_status">
                    %_text = '?'
                    %_tooltip = 'Build status unclear'
                    %if _cell.build_status == 0:
                      %_tooltip = 'Build failed'
                      %_text = '&#x2717;'  # Unicode Character 'BALLOT X'
                      %if _cell.ngood == 0 and _cell.nother == 0 and _cell.nfail == 0:
                        %_text = _text + ' Build Failed'
                      %end
                    %elif _cell.build_status == 1:
                      %_tooltip = 'Build OK'
                      %_text = '&#x2713;'  # Unicode Character 'CHECK MARK'
                    %end
                    <a href="{{ _root }}{{_cell.bld_href}}" title="{{_tooltip}}">{{!_text}}</a>
                  </td>
                  %crashes, crash_count = ([], 0)  # TODO: get crash data
                  %if _cell.nmissing_suites > 0 and _cell.build_status != 0:
                    <td class="bordereditem missing_suites"
                        onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)">
                      <a class="tooltip"
                      href="{{ _root }}/failures/{{ tpl_vars['filter_tag'] }}{{!  query_string(add={'platforms': _platform, 'builds': _build}) }}">
                        {{ _cell.nmissing_suites }}
                        %tooltip(_cell.missing_suites, 'Missing Suites', 'black',  _build, _platform, crash_count, crashes)
                      </a>
                    </td>
                  %end
                  %if _cell.ngood > 0:
                    <td class="bordereditem success"
                        onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)"
                        style="width:{{ 90.0 * _cell.ngood / _cell.ntotal }}%;">
                      <a class="tooltip"
                      href="{{ _root }}/platform/{{ tpl_vars['filter_tag'] }}{{! query_string(add={'platforms': _platform}) }}">
                        {{ _cell.ngood }}
                        %tooltip(None, '', 'black', _build, _platform, crash_count, crashes)
                      </a>
                    </td>
                  %end
                  %if _cell.nfail > 0:
                    <td class="bordereditem failure"
                        onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)"
                        style="width:{{ 90.0 * _cell.nfail / _cell.ntotal }}%;">
                      <a class="tooltip"
                      href="{{ _root }}/failures/{{ tpl_vars['filter_tag'] }}{{!  query_string(add={'platforms': _platform, 'builds': _build}) }}">
                        {{ _cell.nfail }}
                        %tooltip(_cell.failed_tests, 'Failed Tests', 'red',  _build, _platform, crash_count, crashes)
                      </a>
                    </td>
                  %end
                  %if _cell.nother > 0:
                    <td class="bordereditem warning_summary"
                        onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)"
                        style="width:{{ 90.0 * _cell.nother / _cell.ntotal }}%;">
                      <a class="tooltip"
                      href="{{ _root }}/failures/{{ tpl_vars['filter_tag'] }}{{!  query_string(add={'platforms': _platform, 'builds': _build}) }}">
                        {{ _cell.nother }}
                        %tooltip(_cell.problem_tests, 'Problem Tests', 'purple', _build, _platform, crash_count, crashes)
                      </a>
                    </td>
                  %end
                  %if _cell.nmissing_tests > 0:
                    <td class="bordereditem missing_tests"
                        onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)"
                        style="width:{{ 90.0 * _cell.nmissing_tests / _cell.ntotal }}%;">
                      <a class="tooltip"
                      href="{{ _root }}/failures/{{ tpl_vars['filter_tag'] }}{{!  query_string(add={'platforms': _platform, 'builds': _build}) }}">
                        {{ _cell.nmissing_tests }}
                        %tooltip(_cell.missing_tests, 'Missing Tests', 'black',  _build, _platform, crash_count, crashes)
                      </a>
                    </td>
                  %end
                  %if crash_count:
                    <td class="bordereditem warning_summary"
                        onmouseover="fixTooltip(event, this)"
                        onmouseout="clearTooltip(this)">
                      {{ crash_count }}
                    </td>
                  %end
                  </tr>
                  </tbody>
                </table>
              </td>
            %end
          </tr>
        %end
      </tbody>
    </table>
  %end
  </div>
%end

%rebase('master.tpl', title='Test Matrix', query_string=query_string, body_block=body_block)
