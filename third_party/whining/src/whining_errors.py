#!/usr/bin/python
# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Custom exceptions for Whining."""

class WhiningError(Exception):
    """Application base error/exception."""
    pass


class WhiningInputError(WhiningError):
    """Problem with the URL formatting."""
    def __init__(self, msg):
        super(WhiningInputError, self).__init__(msg)


class WhiningDBError(WhiningError):
    """Problem retrieving the data."""
    def __init__(self, msg):
        super(WhiningDBError, self).__init__(msg)


class WhiningFilterError(WhiningError):
    """Problem retrieving the data."""
    def __init__(self, msg):
        super(WhiningFilterError, self).__init__(msg)

class WhiningTypeError(WhiningError):
    """Problem checking type."""
    def __init__(self, msg):
        super(WhiningTypeError, self).__init__(msg)
