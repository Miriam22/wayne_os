GPL Code is copied with explicit permission from Daniel Widyanto:<br>
  http://embeddedfreak.wordpress.com/2010/08/23/howto-use-linux-watchdog/

The project name is based on Samsung's "daisy" reference board design
and I liked this quote from the
[daisy dog FAQ](http://www.daisydogs.com/frequent.php):

> Daisy Dogs are not persnickety little ankle biters

When this daisydog doesn't run, the machine should reset. :)

An alternative code to start with would have been:<br>
  https://dev.openwrt.org/ticket/2270

The [watchdog project](https://sourceforge.net/projects/watchdog/) is alot
more complicated than what the Chromium OS project needs.
