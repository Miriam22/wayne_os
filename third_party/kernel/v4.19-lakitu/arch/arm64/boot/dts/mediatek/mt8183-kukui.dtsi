// SPDX-License-Identifier: (GPL-2.0 OR MIT)
/*
 * Copyright (c) 2018 MediaTek Inc.
 * Author: Ben Ho <ben.ho@mediatek.com>
 *	   Erin Lo <erin.lo@mediatek.com>
 */

#include <dt-bindings/gpio/gpio.h>
#include "mt8183.dtsi"
#include "mt6358.dtsi"

/ {
	aliases {
		serial0 = &uart0;
	};

	backlight_lcd0: backlight_lcd0 {
		compatible = "pwm-backlight";
		pwms = <&pwm0 0 500000>;
		power-supply = <&bl_pp5000>;
		enable-gpios = <&pio 176 0>;
		brightness-levels = <0 1023>;
		num-interpolated-steps = <1023>;
		default-brightness-level = <576>;
		status = "okay";
	};

	memory@40000000 {
		device_type = "memory";
		reg = <0 0x40000000 0 0x80000000>;
	};

	chosen {
		stdout-path = "serial0:115200n8";
	};

	clk32k: oscillator@1 {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <32768>;
		clock-output-names = "clk32k";
	};

	lcd_pp3300: regulator@1 {
		compatible = "regulator-fixed";
		regulator-name = "lcd_pp3300";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;
		regulator-boot-on;
	};

	bl_pp5000: regulator@2 {
		compatible = "regulator-fixed";
		regulator-name = "bl_pp5000";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
		regulator-boot-on;
	};

	mmc1_fixed_power: regulator@3 {
		compatible = "regulator-fixed";
		regulator-name = "mmc1_power";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	mmc1_fixed_io: regulator@4 {
		compatible = "regulator-fixed";
		regulator-name = "mmc1_io";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
	};

	reserved_memory: reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		reserve-memory-sspm_share {
			compatible = "mediatek,reserve-memory-sspm_share";
			no-map;
			status = "okay";
			size = <0 0x110000>; /* 1M + 64K */
			alignment = <0 0x10000>;
			alloc-ranges = <0 0x40000000 0 0x60000000>;
		};

		reserve-memory-vpu_share {
			compatible = "mediatek,reserve-memory-vpu_share";
			no-map;
			size = <0 0x01400000>; /*20 MB share mem size */
			alignment = <0 0x1000000>;
			alloc-ranges = <0 0x50000000 0 0x10000000>;
		};

		scp_mem_reserved: scp_mem_region {
			compatible = "shared-dma-pool";
			reg = <0 0x50000000 0 0x2900000>;
			no-map;
		};
	};

	max98357a: max98357a {
		compatible = "maxim,max98357a";
		sdmode-gpios = <&pio 175 0>;
	};

	btsco: bt-sco {
		compatible = "linux,bt-sco";
	};

	sound: mt8183-mt6358-ts3a227e-max98357a {
		compatible = "mediatek,mt8183_mt6358_ts3a227_max98357";
		mediatek,platform = <&afe>;
		pinctrl-names = "default";
		pinctrl-0 = <&aud_pins>;
		status = "okay";
	};

	wifi_pwrseq: wifi-pwrseq {
		compatible = "mmc-pwrseq-simple";
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_pins_pwrseq>;

		/* Toggle WIFI_ENABLE to reset the chip. */
		reset-gpios = <&pio 119 1>;
	};
};

&afe {
	i2s3-share = "I2S2";
	i2s0-share = "I2S5";
};

&cci {
	proc-supply = <&mt6358_vproc12_reg>;
};

&cpu0 {
	proc-supply = <&mt6358_vproc12_reg>;
};

&cpu1 {
	proc-supply = <&mt6358_vproc12_reg>;
};

&cpu2 {
	proc-supply = <&mt6358_vproc12_reg>;
};

&cpu3 {
	proc-supply = <&mt6358_vproc12_reg>;
};

&cpu4 {
	proc-supply = <&mt6358_vproc11_reg>;
};

&cpu5 {
	proc-supply = <&mt6358_vproc11_reg>;
};

&cpu6 {
	proc-supply = <&mt6358_vproc11_reg>;
};

&cpu7 {
	proc-supply = <&mt6358_vproc11_reg>;
};

&dsi0 {
	status = "okay";
	#address-cells = <1>;
	#size-cells = <0>;
	panel: panel@0 {
		compatible = "innolux,p079zca";
		reg = <0>;
		enable-gpios = <&pio 45 0>;
		pinctrl-names = "default";
		pinctrl-0 = <&panel_pins_default>;
		power-supply = <&lcd_pp3300>;
		backlight = <&backlight_lcd0>;
		status = "okay";
		port {
			panel_in: endpoint {
				remote-endpoint = <&dsi_out>;
			};
		};
	};

	ports {
		port {
			dsi_out: endpoint {
				remote-endpoint = <&panel_in>;
			};
		};
	};
};

&gpu {
	supply-names = "mali","mali_sram";
	mali-supply = <&mt6358_vgpu_reg>;
	mali_sram-supply = <&mt6358_vsram_gpu_reg>;
	operating-points-v2 = <&gpu_opp_table>;
	power_model@0 {
		compatible = "arm,mali-simple-power-model";
		static-coefficient = <2427750>;
		dynamic-coefficient = <4687>;
		ts = <20000 2000 (-20) 2>;
		thermal-zone = "cpu_thermal";
	};
	power_model@1 {
		compatible = "arm,mali-g72-power-model";
		scale = <15000>;
	};
};

&i2c0 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c0_pins>;
	status = "okay";
	clock-frequency = <400000>;
	#address-cells = <1>;
	#size-cells = <0>;
};

&i2c1 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c1_pins>;
	status = "okay";
	clock-frequency = <100000>;
};

&i2c2 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c2_pins>;
	status = "okay";
	clock-frequency = <100000>;
};

&i2c3 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c3_pins>;
	status = "okay";
	clock-frequency = <100000>;
};

&i2c4 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c4_pins>;
	status = "okay";
	clock-frequency = <100000>;
};

&i2c5 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c5_pins>;
	status = "okay";
	clock-frequency = <100000>;
	#address-cells = <1>;
	#size-cells = <0>;

	ts3a227e: ts3a227e@3b {
		pinctrl-names = "default";
		pinctrl-0 = <&ts3a227e_pins>;
		compatible = "ti,ts3a227e";
		reg = <0x3b>;
		interrupt-parent = <&pio>;
		interrupts = <157 IRQ_TYPE_LEVEL_LOW>;

		/*
		 * Only kukui variants with a headphone jack
		 * will enable this node.
		 */
		status = "disabled";
	};
};

&i2c6 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c6_pins>;
	status = "okay";
	clock-frequency = <100000>;
};

&i2c8 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c8_pins>;
	status = "okay";
	clock-frequency = <100000>;
};

&mipi_tx0 {
	status = "okay";
};

&mmc0 {
	status = "okay";
	pinctrl-names = "default", "state_uhs";
	pinctrl-0 = <&mmc0_pins_default>;
	pinctrl-1 = <&mmc0_pins_uhs>;
	bus-width = <8>;
	max-frequency = <200000000>;
	cap-mmc-highspeed;
	mmc-hs200-1_8v;
	mmc-hs400-1_8v;
	cap-mmc-hw-reset;
	no-sdio;
	no-sd;
	hs400-ds-delay = <0x13813>;
	vmmc-supply = <&mt6358_vemc_reg>;
	vqmmc-supply = <&mt6358_vio18_reg>;
	assigned-clocks = <&topckgen CLK_TOP_MUX_MSDC50_0>;
	assigned-clock-parents = <&topckgen CLK_TOP_MSDCPLL_CK>;
	non-removable;
};

&mmc1 {
	status = "okay";
	pinctrl-names = "default", "state_uhs";
	pinctrl-0 = <&mmc1_pins_default>;
	pinctrl-1 = <&mmc1_pins_uhs>;
	vmmc-supply = <&mmc1_fixed_power>;
	vqmmc-supply = <&mmc1_fixed_io>;
	mmc-pwrseq = <&wifi_pwrseq>;
	bus-width = <4>;
	max-frequency = <200000000>;
	drv-type = <2>;
	cap-sd-highspeed;
	sd-uhs-sdr50;
	sd-uhs-sdr104;
	keep-power-in-suspend;
	enable-sdio-wakeup;
	cap-sdio-irq;
	non-removable;
	no-mmc;
	no-sd;
	assigned-clocks = <&topckgen CLK_TOP_MUX_MSDC30_1>;
	assigned-clock-parents = <&topckgen CLK_TOP_MSDCPLL_D2>;
};

&mt6358_vdram2_reg {
	regulator-always-on;
};

&mt6358codec {
	Avdd-supply = <&mt6358_vaud28_reg>;
};

&pio {
	aud_pins: audiopins {
		pins_bus {
			pinmux = <PINMUX_GPIO97__FUNC_I2S2_MCK>,
				<PINMUX_GPIO98__FUNC_I2S2_BCK>,
				<PINMUX_GPIO101__FUNC_I2S2_LRCK>,
				<PINMUX_GPIO102__FUNC_I2S2_DI>,
				<PINMUX_GPIO3__FUNC_I2S3_DO>, /*i2s to da7219/max98357*/
				<PINMUX_GPIO89__FUNC_I2S5_BCK>,
				<PINMUX_GPIO90__FUNC_I2S5_LRCK>,
				<PINMUX_GPIO91__FUNC_I2S5_DO>,
				<PINMUX_GPIO174__FUNC_I2S0_DI>, /*i2s to wifi/bt*/
				<PINMUX_GPIO136__FUNC_AUD_CLK_MOSI>,
				<PINMUX_GPIO137__FUNC_AUD_SYNC_MOSI>,
				<PINMUX_GPIO138__FUNC_AUD_DAT_MOSI0>,
				<PINMUX_GPIO139__FUNC_AUD_DAT_MOSI1>,
				<PINMUX_GPIO140__FUNC_AUD_CLK_MISO>,
				<PINMUX_GPIO141__FUNC_AUD_SYNC_MISO>,
				<PINMUX_GPIO142__FUNC_AUD_DAT_MISO0>,
				<PINMUX_GPIO143__FUNC_AUD_DAT_MISO1>, /*mtkaif3.0*/
				<PINMUX_GPIO169__FUNC_TDM_BCK_2ND>,
				<PINMUX_GPIO170__FUNC_TDM_LRCK_2ND>,
				<PINMUX_GPIO171__FUNC_TDM_DATA0_2ND>,
				<PINMUX_GPIO172__FUNC_TDM_DATA1_2ND>,
				<PINMUX_GPIO173__FUNC_TDM_DATA2_2ND>,
				<PINMUX_GPIO10__FUNC_TDM_DATA3>; /*8ch-i2s to it6505*/
		};
	};

	bt_pins: bt_pins {
		pins_bt_en {
			pinmux = <PINMUX_GPIO120__FUNC_GPIO120>;
			output-low;
		};
	};

	ec_ap_int_odl: ec_ap_int_odl {
		pins1 {
			pinmux = <PINMUX_GPIO151__FUNC_GPIO151>;
			input-enable;
			bias-pull-up;
		};
	};

	h1_int_od_l: h1_int_od_l {
		pins1 {
			pinmux = <PINMUX_GPIO153__FUNC_GPIO153>;
			input-enable;
		};
	};

	i2c0_pins: i2c0 {
		pins_bus {
			pinmux = <PINMUX_GPIO82__FUNC_SDA0>,
				 <PINMUX_GPIO83__FUNC_SCL0>;
			mediatek,pull-up-adv = <3>;
			mediatek,drive-strength-adv = <00>;
		};
	};

	i2c1_pins: i2c1 {
		pins_bus {
			pinmux = <PINMUX_GPIO81__FUNC_SDA1>,
				 <PINMUX_GPIO84__FUNC_SCL1>;
			mediatek,pull-up-adv = <3>;
			mediatek,drive-strength-adv = <00>;
		};
	};

	i2c2_pins: i2c2 {
		pins_bus {
			pinmux = <PINMUX_GPIO103__FUNC_SCL2>,
				 <PINMUX_GPIO104__FUNC_SDA2>;
			bias-disable;
			mediatek,drive-strength-adv = <00>;
		};
	};

	i2c3_pins: i2c3 {
		pins_bus {
			pinmux = <PINMUX_GPIO50__FUNC_SCL3>,
				 <PINMUX_GPIO51__FUNC_SDA3>;
			mediatek,pull-up-adv = <3>;
			mediatek,drive-strength-adv = <00>;
		};
	};

	i2c4_pins: i2c4 {
		pins_bus {
			pinmux = <PINMUX_GPIO105__FUNC_SCL4>,
				 <PINMUX_GPIO106__FUNC_SDA4>;
			bias-disable;
			mediatek,drive-strength-adv = <00>;
		};
	};

	i2c5_pins: i2c5 {
		pins_bus {
			pinmux = <PINMUX_GPIO48__FUNC_SCL5>,
				 <PINMUX_GPIO49__FUNC_SDA5>;
			mediatek,pull-up-adv = <3>;
			mediatek,drive-strength-adv = <00>;
		};
	};

	i2c6_pins: i2c6 {
		pins_bus {
			pinmux = <PINMUX_GPIO11__FUNC_SCL6>,
				 <PINMUX_GPIO12__FUNC_SDA6>;
			bias-disable;
		};
	};

	i2c8_pins: i2c8 {
		pins_bus {
			pinmux = <PINMUX_GPIO108__FUNC_SCL8>,
				 <PINMUX_GPIO109__FUNC_SDA8>;
			bias-disable;
		};
	};

	mmc0_pins_default: mmc0default {
		pins_cmd_dat {
			pinmux = <PINMUX_GPIO123__FUNC_MSDC0_DAT0>,
				 <PINMUX_GPIO128__FUNC_MSDC0_DAT1>,
				 <PINMUX_GPIO125__FUNC_MSDC0_DAT2>,
				 <PINMUX_GPIO132__FUNC_MSDC0_DAT3>,
				 <PINMUX_GPIO126__FUNC_MSDC0_DAT4>,
				 <PINMUX_GPIO129__FUNC_MSDC0_DAT5>,
				 <PINMUX_GPIO127__FUNC_MSDC0_DAT6>,
				 <PINMUX_GPIO130__FUNC_MSDC0_DAT7>,
				 <PINMUX_GPIO122__FUNC_MSDC0_CMD>;
			input-enable;
			drive-strength = <MTK_DRIVE_14mA>;
			mediatek,pull-up-adv = <01>;
		};

		pins_clk {
			pinmux = <PINMUX_GPIO124__FUNC_MSDC0_CLK>;
			drive-strength = <MTK_DRIVE_14mA>;
			mediatek,pull-down-adv = <10>;
		};

		pins_rst {
			pinmux = <PINMUX_GPIO133__FUNC_MSDC0_RSTB>;
			drive-strength = <MTK_DRIVE_14mA>;
			mediatek,pull-down-adv = <01>;
		};
	};

	mmc0_pins_uhs: mmc0@0 {
		pins_cmd_dat {
			pinmux = <PINMUX_GPIO123__FUNC_MSDC0_DAT0>,
				 <PINMUX_GPIO128__FUNC_MSDC0_DAT1>,
				 <PINMUX_GPIO125__FUNC_MSDC0_DAT2>,
				 <PINMUX_GPIO132__FUNC_MSDC0_DAT3>,
				 <PINMUX_GPIO126__FUNC_MSDC0_DAT4>,
				 <PINMUX_GPIO129__FUNC_MSDC0_DAT5>,
				 <PINMUX_GPIO127__FUNC_MSDC0_DAT6>,
				 <PINMUX_GPIO130__FUNC_MSDC0_DAT7>,
				 <PINMUX_GPIO122__FUNC_MSDC0_CMD>;
			input-enable;
			drive-strength = <MTK_DRIVE_14mA>;
			mediatek,pull-up-adv = <01>;
		};

		pins_clk {
			pinmux = <PINMUX_GPIO124__FUNC_MSDC0_CLK>;
			drive-strength = <MTK_DRIVE_14mA>;
			mediatek,pull-down-adv = <10>;
		};

		pins_ds {
			pinmux = <PINMUX_GPIO131__FUNC_MSDC0_DSL>;
			drive-strength = <MTK_DRIVE_14mA>;
			mediatek,pull-down-adv = <10>;
		};

		pins_rst {
			pinmux = <PINMUX_GPIO133__FUNC_MSDC0_RSTB>;
			drive-strength = <MTK_DRIVE_14mA>;
			mediatek,pull-up-adv = <01>;
		};
	};

	mmc1_pins_default: mmc1default {
		pins_cmd_dat {
			pinmux = <PINMUX_GPIO31__FUNC_MSDC1_CMD>,
				 <PINMUX_GPIO32__FUNC_MSDC1_DAT0>,
				 <PINMUX_GPIO34__FUNC_MSDC1_DAT1>,
				 <PINMUX_GPIO33__FUNC_MSDC1_DAT2>,
				 <PINMUX_GPIO30__FUNC_MSDC1_DAT3>;
			input-enable;
			mediatek,pull-up-adv = <10>;
		};

		pins_clk {
			pinmux = <PINMUX_GPIO29__FUNC_MSDC1_CLK>;
			input-enable;
			mediatek,pull-down-adv = <10>;
		};
	};

	mmc1_pins_uhs: mmc1@0 {
		pins_cmd_dat {
			pinmux = <PINMUX_GPIO31__FUNC_MSDC1_CMD>,
				 <PINMUX_GPIO32__FUNC_MSDC1_DAT0>,
				 <PINMUX_GPIO34__FUNC_MSDC1_DAT1>,
				 <PINMUX_GPIO33__FUNC_MSDC1_DAT2>,
				 <PINMUX_GPIO30__FUNC_MSDC1_DAT3>;
			drive-strength = <MTK_DRIVE_6mA>;
			input-enable;
			mediatek,pull-up-adv = <10>;
		};

		pins_clk {
			pinmux = <PINMUX_GPIO29__FUNC_MSDC1_CLK>;
			drive-strength = <MTK_DRIVE_8mA>;
			mediatek,pull-down-adv = <10>;
			input-enable;
		};
	};

	panel_pins_default: panel_pins_default {
		panel_reset {
			pinmux = <PINMUX_GPIO45__FUNC_GPIO45>;
			output-low;
			bias-pull-up;
		};
	};

	pwm0_pin_default: pwm0_pin_default {
		pins1 {
			pinmux = <PINMUX_GPIO176__FUNC_GPIO176>;
			output-high;
			bias-pull-up;
		};
		pins2 {
			pinmux = <PINMUX_GPIO43__FUNC_DISP_PWM>;
		};
	};

	scp_pins: scp {
		pins_eint {
			pinmux = <PINMUX_GPIO5__FUNC_TP_GPIO5_AO>,
				 <PINMUX_GPIO6__FUNC_TP_GPIO6_AO>,
				 <PINMUX_GPIO7__FUNC_TP_GPIO7_AO>;
		};

		pins_scp_uart {
			pinmux = <PINMUX_GPIO110__FUNC_TP_URXD1_AO>,
				 <PINMUX_GPIO112__FUNC_TP_UTXD1_AO>;
		};
	};

	spi_pins_0: spi0@0 {
		pins_spi{
			pinmux = <PINMUX_GPIO85__FUNC_SPI0_MI>,
				 <PINMUX_GPIO86__FUNC_GPIO86>,
				 <PINMUX_GPIO87__FUNC_SPI0_MO>,
				 <PINMUX_GPIO88__FUNC_SPI0_CLK>;
			bias-disable;
		};
	};

	spi_pins_1: spi1@0 {
		pins_spi{
			pinmux = <PINMUX_GPIO161__FUNC_SPI1_A_MI>,
				 <PINMUX_GPIO162__FUNC_SPI1_A_CSB>,
				 <PINMUX_GPIO163__FUNC_SPI1_A_MO>,
				 <PINMUX_GPIO164__FUNC_SPI1_A_CLK>;
			bias-disable;
		};
	};

	spi_pins_2: spi2@0 {
		pins_spi{
			pinmux = <PINMUX_GPIO0__FUNC_SPI2_CSB>,
				 <PINMUX_GPIO1__FUNC_SPI2_MO>,
				 <PINMUX_GPIO2__FUNC_SPI2_CLK>;
			bias-disable;
		};
		pins_spi_mi {
			pinmux = <PINMUX_GPIO94__FUNC_SPI2_MI>;
			mediatek,pull-down-adv = <00>;
		};
	};

	spi_pins_3: spi3@0 {
		pins_spi{
			pinmux = <PINMUX_GPIO21__FUNC_SPI3_MI>,
				 <PINMUX_GPIO22__FUNC_SPI3_CSB>,
				 <PINMUX_GPIO23__FUNC_SPI3_MO>,
				 <PINMUX_GPIO24__FUNC_SPI3_CLK>;
			bias-disable;
		};
	};

	spi_pins_4: spi4@0 {
		pins_spi{
			pinmux = <PINMUX_GPIO17__FUNC_SPI4_MI>,
				 <PINMUX_GPIO18__FUNC_SPI4_CSB>,
				 <PINMUX_GPIO19__FUNC_SPI4_MO>,
				 <PINMUX_GPIO20__FUNC_SPI4_CLK>;
			bias-disable;
		};
	};

	spi_pins_5: spi5@0 {
		pins_spi{
			pinmux = <PINMUX_GPIO13__FUNC_SPI5_MI>,
				 <PINMUX_GPIO14__FUNC_SPI5_CSB>,
				 <PINMUX_GPIO15__FUNC_SPI5_MO>,
				 <PINMUX_GPIO16__FUNC_SPI5_CLK>;
			bias-disable;
		};
	};

	ts3a227e_pins: ts3a227e_pins {
		pins1 {
			pinmux = <PINMUX_GPIO157__FUNC_GPIO157>;
			input-enable;
			bias-enable;
			bias-pull-up;
		};
	};

	uart0_pin: uart0default {
		pins_rx {
			pinmux = <PINMUX_GPIO95__FUNC_URXD0>;
			input-enable;
			bias-pull-up;
		};
		pins_tx {
			pinmux = <PINMUX_GPIO96__FUNC_UTXD0>;
		};
	};

	uart1_pin: uart1default {
		pins_rx {
			pinmux = <PINMUX_GPIO121__FUNC_URXD1>;
			input-enable;
			bias-pull-up;
		};
		pins_tx {
			pinmux = <PINMUX_GPIO115__FUNC_UTXD1>;
		};
		pins_rts {
			pinmux = <PINMUX_GPIO47__FUNC_URTS1>;
			output-enable;
		};
		pins_cts {
			pinmux = <PINMUX_GPIO46__FUNC_UCTS1>;
			input-enable;
		};
	};

	wifi_pins_pwrseq: wifipwrseq {
		pins_wifi_enable {
			pinmux = <PINMUX_GPIO119__FUNC_GPIO119>;
			output-low;
		};
	};
};

&pwm0 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&pwm0_pin_default>;
};

&scp {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&scp_pins>;

	cros_ec {
		compatible = "google,cros-ec-rpmsg";
		mtk,rpmsg-name = "cros-ec-rpmsg";
	};
};

&scpsys {
	mfg-supply = <&mt6358_vgpu_reg>;
};

&soc_data {
	status = "okay";
};

&spi0 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi_pins_0>;
	mediatek,pad-select = <0>;
	status = "okay";
	cs-gpios = <&pio 86 GPIO_ACTIVE_LOW>;

	cr50@0 {
		compatible = "google,cr50";
		reg = <0>;
		spi-max-frequency = <1000000>;
		pinctrl-names = "default";
		pinctrl-0 = <&h1_int_od_l>;
		interrupt-parent = <&pio>;
		interrupts = <153 IRQ_TYPE_EDGE_RISING>;
	};
};

&spi1 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi_pins_1>;
	mediatek,pad-select = <0>;
	status = "okay";

	spi@0 {
		compatible = "winbond,w25q64dw", "jedec,spi-nor";
		reg = <0>;
		spi-max-frequency = <25000000>;
	};
};

&spi2 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi_pins_2>;
	mediatek,pad-select = <0>;
	status = "okay";

	cros_ec: ec@0 {
		compatible = "google,cros-ec-spi";
		reg = <0>;
		spi-max-frequency = <3000000>;
		interrupt-parent = <&pio>;
		interrupts = <151 IRQ_TYPE_LEVEL_LOW>;
		pinctrl-names = "default";
		pinctrl-0 = <&ec_ap_int_odl>;

		i2c_tunnel: i2c-tunnel {
			compatible = "google,cros-ec-i2c-tunnel";
			google,remote-bus = <1>;
			#address-cells = <1>;
			#size-cells = <0>;
		};
	};
};

&spi3 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi_pins_3>;
	mediatek,pad-select = <0>;
	status = "disabled";

	spidev3: spi@0 {
		compatible = "linux,spidev";
		reg = <0>;
		spi-max-frequency = <1000000>;
	};
};

&spi4 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi_pins_4>;
	mediatek,pad-select = <0>;
	status = "disabled";

	spidev4: spi@0 {
		compatible = "linux,spidev";
		reg = <0>;
		spi-max-frequency = <1000000>;
	};
};

&spi5 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi_pins_5>;
	mediatek,pad-select = <0>;
	status = "disabled";

	spidev5: spi@0 {
		compatible = "linux,spidev";
		reg = <0>;
		spi-max-frequency = <1000000>;
	};
};

&ssusb {
	dr_mode = "host";
	vusb33-supply = <&mt6358_vusb_reg>;
	status = "okay";
};

&usb_host {
	vusb33-supply = <&mt6358_vusb_reg>;
	status = "okay";
};

&u3phy {
	status = "okay";
};

&uart0 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart0_pin>;
	status = "okay";
};

&uart1 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart1_pin>;
	status = "okay";

	bluetooth: qca6174-bt {
		pinctrl-names = "default";
		pinctrl-0 = <&bt_pins>;
		status = "okay";
		compatible = "qcom,qca6174-bt";
		enable-gpios = <&pio 120 0>;
		clocks = <&clk32k>;
		firmware-name = "nvm_00440302_i2s.bin";
	};
};

#include <arm/cros-ec-keyboard.dtsi>
#include <arm/cros-ec-sbs.dtsi>

