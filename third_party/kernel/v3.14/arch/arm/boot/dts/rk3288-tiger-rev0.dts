/*
 * Google Veyron Tiger Rev 0+ board device tree source
 *
 * Copyright 2016 Google, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/dts-v1/;
#include "rk3288-veyron.dtsi"
#include "rk3288-veyron-analog-audio.dtsi"
#include "rk3288-veyron-edp.dtsi"
#include "rk3288-veyron-ethernet.dtsi"
#include "rk3288-veyron-lpddr.dtsi"

/ {
	model = "Google Tiger";
	compatible = "google,veyron-tiger-rev8", "google,veyron-tiger-rev7",
		     "google,veyron-tiger-rev6", "google,veyron-tiger-rev5",
		     "google,veyron-tiger-rev4", "google,veyron-tiger-rev3",
		     "google,veyron-tiger-rev2", "google,veyron-tiger-rev1",
		     "google,veyron-tiger-rev0", "google,veyron-tiger",
		     "google,veyron", "rockchip,rk3288";

	backlight_regulator: backlight-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio2 12 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&bl_pwr_en>;
		regulator-name = "backlight_regulator";
		vin-supply = <&vccsys>;
		startup-delay-us = <15000>;
	};

	panel_regulator: panel-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio7 14 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&lcd_enable_h>;
		regulator-name = "panel_regulator";
		vin-supply = <&vcc33_sys>;
	};

	/* vcc33_pmuio and vcc33_io is sourced directly from vcc33_sys,
	 * enabled by vcc_18 */
	vcc33_io: vcc33-io {
		compatible = "regulator-fixed";
		regulator-always-on;
		regulator-boot-on;
		regulator-name = "vcc33_io";
	};

	vcc5_host1: vcc5-host1-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio5 18 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&hub_usb1_pwr_en>;
		regulator-name = "vcc5_host1";
		regulator-always-on;
		regulator-boot-on;
	};

	vcc5_host2: vcc5-host2-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio5 14 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&hub_usb2_pwr_en>;
		regulator-name = "vcc5_host2";
		regulator-always-on;
		regulator-boot-on;
	};

	vcc5v_otg: vcc5v-otg-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio0 12 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&usb_otg_pwr_en>;
		regulator-name = "vcc5_otg";
		regulator-always-on;
		regulator-boot-on;
	};

	vccsys: vccsys {
		compatible = "regulator-fixed";
		regulator-name = "vccsys";
		regulator-boot-on;
		regulator-always-on;
	};
};

&backlight {
	/* Tiger panel PWM must be >= 1%, so start non-zero brightness at 3 */
	brightness-levels = <
			  0   3   4   5   6   7
			  8   9  10  11  12  13  14  15
			 16  17  18  19  20  21  22  23
			 24  25  26  27  28  29  30  31
			 32  33  34  35  36  37  38  39
			 40  41  42  43  44  45  46  47
			 48  49  50  51  52  53  54  55
			 56  57  58  59  60  61  62  63
			 64  65  66  67  68  69  70  71
			 72  73  74  75  76  77  78  79
			 80  81  82  83  84  85  86  87
			 88  89  90  91  92  93  94  95
			 96  97  98  99 100 101 102 103
			104 105 106 107 108 109 110 111
			112 113 114 115 116 117 118 119
			120 121 122 123 124 125 126 127
			128 129 130 131 132 133 134 135
			136 137 138 139 140 141 142 143
			144 145 146 147 148 149 150 151
			152 153 154 155 156 157 158 159
			160 161 162 163 164 165 166 167
			168 169 170 171 172 173 174 175
			176 177 178 179 180 181 182 183
			184 185 186 187 188 189 190 191
			192 193 194 195 196 197 198 199
			200 201 202 203 204 205 206 207
			208 209 210 211 212 213 214 215
			216 217 218 219 220 221 222 223
			224 225 226 227 228 229 230 231
			232 233 234 235 236 237 238 239
			240 241 242 243 244 245 246 247
			248 249 250 251 252 253 254 255>;
	power-supply = <&backlight_regulator>;
};

&dmc {
	status = "disabled";
};

&edp {
	pinctrl-names = "default";
	pinctrl-0 = <&edp_hpd>;
};

&gmac {
	phy-supply = <&vcc33_lan>;
	wakeup-source;
	mdio0 {
		#address-cells = <1>;
		#size-cells = <0>;
		compatible = "snps,dwmac-mdio";
		phy1: ethernet-phy@1 {
			reg = <1>;
			enable-phy-ssc;
		};
	};
};

&gpio_keys {
	pinctrl-0 = <&pwr_key_l>;

	/* let btmrvl handle bt wake */
	/delete-node/ bt-wake;
};

&i2c3 {
	status = "okay";

	clock-frequency = <400000>;
	i2c-scl-falling-time-ns = <50>;
	i2c-scl-rising-time-ns = <300>;

	touchscreen@10 {
		compatible = "elan,ekth3912", "elan,ekth3500";
		reg = <0x10>;
		pinctrl-names = "default";
		pinctrl-0 = <&touch_int &touch_rst>;
		interrupt-parent = <&gpio2>;
		interrupts = <14 IRQ_TYPE_EDGE_FALLING>;
		reset-gpios = <&gpio2 15 GPIO_ACTIVE_LOW>;
		vcc33-supply = <&vcc33_io>;
		vccio-supply = <&vcc33_io>;
		wakeup-source;
	};
};

&panel {
	compatible = "auo,b101ean01", "simple-panel";
	power-supply= <&panel_regulator>;
};

&rk808 {
	pinctrl-names = "default";
	pinctrl-0 = <&pmic_int_l &dvs_1 &dvs_2>;
	dvs-gpios = <&gpio7 12 GPIO_ACTIVE_HIGH>,
		    <&gpio7 15 GPIO_ACTIVE_HIGH>;

	vcc6-supply = <&vcc33_sys>;
	vcc10-supply = <&vcc33_sys>;
	vcc11-supply = <&vcc_5v>;
	vcc12-supply = <&vcc33_sys>;

	regulators {
		/delete-node/ LDO_REG1;

		vcc18_lcdt: LDO_REG2 {
			regulator-always-on;
			regulator-boot-on;
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			regulator-name = "vdd18_lcdt";
			regulator-suspend-mem-disabled;
		};

		/* This is not a pwren anymore, but the real power supply */
		vdd10_lcd: LDO_REG7 {
			regulator-always-on;
			regulator-boot-on;
			regulator-min-microvolt = <1000000>;
			regulator-max-microvolt = <1000000>;
			regulator-name = "vdd10_lcd";
			regulator-suspend-mem-disabled;
		};

		/* for usb camera */
		vcc33_ccd: LDO_REG8 {
			regulator-always-on;
			regulator-boot-on;
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
			regulator-name = "vcc33_ccd";
			regulator-suspend-mem-disabled;
		};

		vcc33_lan: SWITCH_REG2 {
			regulator-name = "vcc33_lan";
		};
	};
};

&sdio0 {
	#address-cells = <1>;
	#size-cells = <0>;

	btmrvl: btmrvl@2 {
		compatible = "marvell,sd8897-bt";
		reg = <2>;
		interrupt-parent = <&gpio4>;
		interrupts = <31 IRQ_TYPE_LEVEL_LOW>;
		marvell,wakeup-pin = /bits/ 16 <13>;
		pinctrl-names = "default";
		pinctrl-0 = <&bt_host_wake_l>;
	};
};

&vcc50_hdmi {
	enable-active-high;
	gpio = <&gpio5 19 GPIO_ACTIVE_HIGH>;
	pinctrl-names = "default";
	pinctrl-0 = <&vcc50_hdmi_en>;
};

&vcc_5v {
	enable-active-high;
	gpio = <&gpio7 21 GPIO_ACTIVE_HIGH>;
	pinctrl-names = "default";
	pinctrl-0 = <&drv_5v>;
};

&pinctrl {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <
		/* Common for sleep and wake, but no owners */
		&ddr0_retention
		&ddrio_pwroff
		&global_pwroff

		/* For usb bc1.2 */
		&usb_otg_ilim_sel
		&usb_usb_ilim_sel

		/* Wake only */
		&bt_dev_wake_awake
		&pwr_led1_on
	>;
	pinctrl-1 = <
		/* Common for sleep and wake, but no owners */
		&ddr0_retention
		&ddrio_pwroff
		&global_pwroff

		/* Sleep only */
		&bt_dev_wake_sleep
		&pwr_led1_blink
	>;

	backlight {
		bl_pwr_en: bl_pwr_en {
			rockchip,pins = <2 12 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	buck-5v {
		drv_5v: drv-5v {
			rockchip,pins = <7 21 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	edp {
		edp_hpd: edp-hpd {
			rockchip,pins = <7 11 RK_FUNC_2 &pcfg_pull_down>;
		};
	};

	hdmi {
		vcc50_hdmi_en: vcc50-hdmi-en {
			rockchip,pins = <5 19 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	lcd {
		lcd_enable_h: lcd-en {
			rockchip,pins = <7 14 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	leds {
		pwr_led1_on: pwr-led1-on {
			rockchip,pins = <7 3 RK_FUNC_GPIO &pcfg_output_low>;
		};

		pwr_led1_blink: pwr-led1-blink {
			rockchip,pins = <7 3 RK_FUNC_GPIO &pcfg_output_high>;
		};
	};

	pmic {
		dvs_1: dvs-1 {
			rockchip,pins = <7 12 RK_FUNC_GPIO &pcfg_pull_down>;
		};

		dvs_2: dvs-2 {
			rockchip,pins = <7 15 RK_FUNC_GPIO &pcfg_pull_down>;
		};
	};

	touchscreen {
		touch_int: touch-int {
			rockchip,pins = <2 14 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		touch_rst: touch-rst {
			rockchip,pins = <2 15 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	usb-bc12 {
		usb_otg_ilim_sel: usb-otg-ilim-sel {
			rockchip,pins = <6 17 RK_FUNC_GPIO &pcfg_output_low>;
		};

		usb_usb_ilim_sel: usb-usb-ilim-sel {
			rockchip,pins = <5 15 RK_FUNC_GPIO &pcfg_output_low>;
		};
	};

	usb-host {
		hub_usb1_pwr_en: hub_usb1_pwr_en {
			rockchip,pins = <5 18 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		hub_usb2_pwr_en: hub_usb2_pwr_en {
			rockchip,pins = <5 14 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		usb_otg_pwr_en: usb_otg_pwr_en {
			rockchip,pins = <0 12 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
};
