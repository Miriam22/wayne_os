# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description     "Kerberos daemon"
author          "chromium-os-dev@chromium.org"

# The service is started by Chrome on demand.
stop on stopping ui
respawn

# Minijail actually forks off the desired process.
expect fork

script
  # Start constructing minijail0 args...
  args=""

  # Make sure minijail0 exits right away and won't block upstart.
  args="${args} -i"

  # Create a UTS namespace to isolate changes to the host / domain name.
  args="${args} --uts"

  # Create a PID namespace (process won't see any other processes).
  args="${args} -p"

  # Create an IPC namespace (isolate System V IPC objects/POSIX message queues).
  args="${args} -l"

  # Creates new, empty tmp directory (technically, mounts tmpfs).
  args="${args} -t"

  # Prevent that execve gains privileges, required for seccomp filters.
  args="${args} -n"

  # Use a minimalistic mount namespace.
  args="${args} --profile minimalistic-mountns"

  # Bind-mount /run read-only for D-Bus to work.
  args="${args} -b /run"

  # Run as kerberosd user and group.
  args="${args} -u kerberosd -g kerberosd"

  # Execute kerberosd.
  args="${args} /usr/sbin/kerberosd"

  # -e is not specified because the service needs to connect to servers.

  exec minijail0 ${args}
end script

# Wait for daemon to claim its D-Bus name before transitioning to started.
post-start exec minijail0 -u kerberosd -g kerberosd /usr/bin/gdbus \
    wait --system --timeout 15 org.chromium.Kerberos
